﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Resources;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Q1
{
    public partial class WarForm : Form
    {
        private PictureBox[] myCards;
        private PictureBox opCard;
        private PictureBox mySelectedCard; // will store the card was currently clicked
        private int myScore, opScore;
        private Thread backgroundThread;
        private TcpClient client;
        private NetworkStream clientStream;
        private string myTurnCard, opTurnCard;


        public WarForm()
        {
            InitializeComponent();

            GenerateCards(); // Generating the game cards
            // Starting the background
            backgroundThread = new Thread(BackgroundThread);
            backgroundThread.Start();
            FreezeGame(true); // freezing game until background process changes
            // Resetting variables
            client = new TcpClient();
            myScore = 0;
            opScore = 0;
            myTurnCard = "";
            opTurnCard = "";
        }

        
        

        private void WarForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundThread.Abort();
            // trying to shut down game properly, if server is down simply exit
            try
            {
                clientStream.Write(Encoding.ASCII.GetBytes("2000"), 0, 4);
                clientStream.Flush();
                client.Close();
            }
            catch { }
            Environment.Exit(0);
        }

        /// <summary>
        /// Generates the cards in the GUI
        /// </summary>
        private void GenerateCards()
        {
            // Variable initiation
            myCards = new PictureBox[10];
            myScore = 0;
            opScore = 0;
            // Variable computing
            int cardWidth = 165, cardHeight = (int)(cardWidth * 1.55);
            int locationY = this.Height - cardHeight * 5 / 7 - 20;
            int incrementX = (this.Width - cardWidth - 4) / (myCards.Length - 1);
            int middleSection = (this.Width - cardWidth - 8) / 2;
            // Creating all cards
            for(int i = 0; i < myCards.Length; i++)
            {
                myCards[i] = new PictureBox();
                myCards[i].Name = "Card" + i;
                myCards[i].Location = new Point(incrementX * i - 4, locationY);
                myCards[i].Size = new Size(cardWidth, cardHeight);
                myCards[i].Click += CardClick;
                myCards[i].BackgroundImage = Q1.Properties.Resources.card_back_red;
                myCards[i].BackgroundImageLayout = ImageLayout.Stretch;
                this.Controls.Add(myCards[i]);
                myCards[i].BringToFront();
            }
            // Creating opponent card
            opCard = new PictureBox();
            opCard.Name = "OpponentCard";
            opCard.Location = new Point(middleSection, cardHeight * 2 / 7);
            opCard.Size = new Size(cardWidth, cardHeight);
            opCard.BackgroundImage = Q1.Properties.Resources.card_back_blue;
            opCard.BackgroundImageLayout = ImageLayout.Stretch;
            this.Controls.Add(opCard);
        }

        /// <summary>
        /// Disables all user cards or enables if requested
        /// </summary>
        /// <param name="toDisable"></param>
        private void DisableCards(bool toDisable)
        {
            for (int i = 0; i < myCards.Length; i++)
            {
                myCards[i].Enabled = !toDisable;
            }
        }


        public void CardClick(object sender, EventArgs e)
        {
            string card = GetRandomCard(); // Getting a random card in the used format
            // Setting selected card and displaying it
            mySelectedCard = (PictureBox)sender;
            SetSelectedCard(mySelectedCard, "1" + card);
            myTurnCard = "1" + card; // Setting client card
            // Sending card to opponent
            clientStream.Write((Encoding.ASCII.GetBytes("1" + card)), 0, 4);
            clientStream.Flush();
            DisableCards(true); // Disabling cards until other user selects
            // Checking and trying to finish round if both clients have finished
            CheckDoRound(); 
        }
        

        /// <summary>
        /// Generates a random card
        /// </summary>
        /// <returns>Returns a card in the range of the 52 cards in deck</returns>
        private string GetRandomCard()
        {
            /* order is number then shape&color: number is selected from 1-13 and then multiplied 13 for the shape location */
            // Variables
            int cardNum = new Random().Next(1, 53);
            int shape = 0;
            char[] shapes = new char[] { 'C', 'D', 'H', 'S' }; // Clubs, Diamonds, Hearts, Spades

            for (shape = 4; shape > 0 && cardNum > 13; shape--, cardNum -= 13) ; // getting shape
            return String.Format("{0:00}{1}",cardNum, shapes[shape - 1]);

        }
        /// <summary>
        /// Sets the middle card to the requested card by format
        /// </summary>
        /// <param name="cardData">the string containing the data received of the card</param>
        private void SetSelectedCard(PictureBox card, string cardData)
        {
            // Variables
            char color = cardData[3];
            int num = int.Parse(cardData.Substring(1, 2));
            string fileName = "_";
            // Getting number
            if(num == 1)
            {
                fileName += "ace";
            }
            else if(num > 10)
            {
                fileName += (new string[] { "jack", "queen", "king" })[num - 11];
            }
            else
            {
                fileName += num.ToString();
            }
            
            fileName += "_of_";
            
            // Getting color
            switch(color)
            {
                case 'C':
                    fileName += "clubs";
                    break;
                case 'D':
                    fileName += "diamonds";
                    break;
                case 'H':
                    fileName += "hearts";
                    break;
                case 'S':
                    fileName += "spades";
                    break;
            }

            //fileName += ".png";

            // Setting card to be image
            ResourceManager rm = Properties.Resources.ResourceManager;
            
            card.BackgroundImage = (Image)rm.GetObject(fileName);
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Checking if both clients have selected a card, if so - continues
        /// </summary>
        private void CheckDoRound()
        {
            // Executing function only if both clients have played
            if(myTurnCard != "" && opTurnCard != "")
            {
                // comparing numbers through string
                // case: Winner = me
                if(myTurnCard[1] > opTurnCard[1] || (myTurnCard[1] == opTurnCard[1] && myTurnCard[2] > opTurnCard[2]))
                {
                    myScore++;
                    myScoreLbl.Text = "Your Score: " + myScore.ToString();
                }
                // case: Winner = opponent
                else if(!(myTurnCard[1] == opTurnCard[1] && myTurnCard[2] == opTurnCard[2]))
                {
                    opScore++;
                    opScoreLbl.Text = "Opponent Score: " + opScore.ToString();
                }
                Thread.Sleep(300);
                // Resetting to next round
                mySelectedCard.BackgroundImage = Properties.Resources.card_back_red;
                opCard.BackgroundImage = Properties.Resources.card_back_blue;
                mySelectedCard = null;
                myTurnCard = "";
                opTurnCard = "";
                DisableCards(false);
            }
        }

        
        /// <summary>
        /// Sets all cards to be enable or not
        /// </summary>
        /// <param name="isDisable">Indicating the enable state to set the cards</param>
        private void FreezeGame(bool toFreeze)
        {
            if (toFreeze)
            {
                for (int i = 0; i < myCards.Length; i++)
                {
                    myCards[i].Visible = false;
                }
            }
            else
            {
                for(int i = 0; i < myCards.Length; i++)
                {
                    myCards[i].Visible = true;
                    myCards[i].BringToFront();
                }
            }
            opCard.Visible = !toFreeze;
            ExitBtn.Visible = !toFreeze;
            waitLbl.Visible = toFreeze;
        }



        /*** BACKGROUND THREAD ***/

        /// <summary>
        /// Background thread managing server communication
        /// </summary>
        private void BackgroundThread()
        {
            // Variables
            string recvData = "";
            byte[] data = new byte[1024];
            //soc = new Socket(AddressFamily.Unknown, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint addr = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

            // Trying to connect to server
            while (true)
            {
                // Trying to connect until host found
                try
                {
                    client.Connect(addr);
                    clientStream = client.GetStream();
                    break;
                }
                catch
                {
                    Thread.Sleep(100); // Checking again every .1 seconds
                }
            }

            clientStream.Read(data, 0, 4);
            // Checking if message received was correct
            recvData = Encoding.ASCII.GetString(data, 0, 4);
            if (recvData != "0000")
            {
                if (MessageBox.Show("Something went wrong. please try again later", "Server ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    this.Close();
                }
            }

            Invoke((MethodInvoker)(() => FreezeGame(false))); // Enabling all cards

            // Running game - waiting for card or game ending
            while (true)
            {
                // Getting card selection from user
                try // trying to connect - will crash if server stopped
                {
                    clientStream.Read(data, 0, 4);
                }
                catch
                {
                    this.Hide();
                    // Preparing message to user before exit
                    string msg = "Server Was Stopped.\n\n--RESULT--\n";
                    msg += "Your Score: " + myScore;
                    msg += "\nOpponent Score: " + opScore;
                    if (MessageBox.Show(msg, "Game Ended - Server Stopped", MessageBoxButtons.OK, MessageBoxIcon.None) == DialogResult.OK)
                    {
                        Environment.Exit(0);
                    }
                }
                recvData = Encoding.ASCII.GetString(data);
                // 1 = card
                if (recvData[0] == '1')
                {
                    opTurnCard = Encoding.ASCII.GetString(data);
                    SetSelectedCard(opCard, opTurnCard);
                    Invoke((MethodInvoker)CheckDoRound); // attepting to finish round if both clients have finished
                }
                // 2 = game end
                else if (recvData[0] == '2')
                {
                    string msg = "Opponent Left.\n\n--RESULT--\n";
                    msg += "Your Score: " + myScore;
                    msg += "\nOpponent Score: " + opScore;
                    if (MessageBox.Show(msg, "Game Ended - Opponent Left", MessageBoxButtons.OK, MessageBoxIcon.None) == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
            }
        }
    }
}
