﻿namespace Q1
{
    partial class WarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExitBtn = new System.Windows.Forms.Button();
            this.myScoreLbl = new System.Windows.Forms.Label();
            this.opScoreLbl = new System.Windows.Forms.Label();
            this.waitLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ExitBtn
            // 
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ExitBtn.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.Location = new System.Drawing.Point(499, -1);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(107, 44);
            this.ExitBtn.TabIndex = 0;
            this.ExitBtn.Text = "Quit";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // myScoreLbl
            // 
            this.myScoreLbl.AutoSize = true;
            this.myScoreLbl.Font = new System.Drawing.Font("Cambria Math", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myScoreLbl.Location = new System.Drawing.Point(35, -37);
            this.myScoreLbl.Name = "myScoreLbl";
            this.myScoreLbl.Size = new System.Drawing.Size(169, 117);
            this.myScoreLbl.TabIndex = 1;
            this.myScoreLbl.Text = "Your Score: 0";
            // 
            // opScoreLbl
            // 
            this.opScoreLbl.AutoSize = true;
            this.opScoreLbl.Font = new System.Drawing.Font("Cambria Math", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opScoreLbl.Location = new System.Drawing.Point(868, -37);
            this.opScoreLbl.Name = "opScoreLbl";
            this.opScoreLbl.Size = new System.Drawing.Size(217, 117);
            this.opScoreLbl.TabIndex = 2;
            this.opScoreLbl.Text = "Opponent Score: 0";
            // 
            // waitLbl
            // 
            this.waitLbl.AutoSize = true;
            this.waitLbl.Font = new System.Drawing.Font("Cooper Black", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitLbl.Location = new System.Drawing.Point(499, 318);
            this.waitLbl.Name = "waitLbl";
            this.waitLbl.Size = new System.Drawing.Size(106, 24);
            this.waitLbl.TabIndex = 3;
            this.waitLbl.Text = "Waiting..";
            // 
            // WarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(1104, 661);
            this.Controls.Add(this.waitLbl);
            this.Controls.Add(this.opScoreLbl);
            this.Controls.Add(this.myScoreLbl);
            this.Controls.Add(this.ExitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WarForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "War";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WarForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Label myScoreLbl;
        private System.Windows.Forms.Label opScoreLbl;
        private System.Windows.Forms.Label waitLbl;

    }
}

